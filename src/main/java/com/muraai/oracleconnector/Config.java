package com.muraai.oracleconnector;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@PropertySource(value = "classpath:application.properties")
@EnableJpaRepositories(Constant.REPOSITORY_PACKAGE_NAME)
@EnableTransactionManagement
public class Config {
	
	@Autowired
	private Environment env;

	@Autowired
	JpaVendorAdapter jpaVendorAdapter;

	
	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("ap.datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("ap.datasource.url"));
		dataSource.setUsername(env.getProperty("ap.datasource.username"));
		dataSource.setPassword(env.getProperty("ap.datasource.password"));

		return dataSource;
	}

	@Bean
	LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, Environment env) {

		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		entityManagerFactoryBean.setPackagesToScan(Constant.MODEL_PACKAGE_NAME);

		Properties jpaProperties = new Properties();

		// Configures the used database dialect. This allows Hibernate to create
		// SQL
		// that is optimized for the used database.
		jpaProperties.put(Constant.HIBERNATE_DIALECT, env.getRequiredProperty(Constant.HIBERNATE_DIALECT));

		// Specifies the action that is invoked to the database when the
		// Hibernate
		// SessionFactory is created or closed.
		jpaProperties.put(Constant.HIBERNATE_HBM2_DDL, env.getRequiredProperty(Constant.HIBERNATE_HBM2_DDL));

		// Configures the naming strategy that is used when Hibernate creates
		// new database objects and schema elements
		jpaProperties.put(Constant.HIBERNATE_EJB_NAMING_STRATEGY,
				env.getRequiredProperty(Constant.HIBERNATE_EJB_NAMING_STRATEGY));

		// If the value of this property is true, Hibernate writes all SQL
		// statements to the console.
		jpaProperties.put(Constant.HIBERNATE_SHOW_SQL, env.getRequiredProperty(Constant.HIBERNATE_SHOW_SQL));

		// If the value of this property is true, Hibernate will format the SQL
		// that is written to the console.
		jpaProperties.put(Constant.HIBERNATE_FORMAT_SQL, env.getRequiredProperty(Constant.HIBERNATE_FORMAT_SQL));

		entityManagerFactoryBean.setJpaProperties(jpaProperties);

		return entityManagerFactoryBean;

	}

	@Bean
	public JpaTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory(dataSource(), env).getObject());
		return transactionManager;
	}


}
