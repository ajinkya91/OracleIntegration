package com.muraai.oracleconnector.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="erp_po_header")
public class ErpPoHeader implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="po_no")
	private String poNo;

	@Id
	@Column(name="erp_po_header_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private BigInteger erpPoHeaderId;

	@Column(name="po_purch_grp")
	private String poPurchGrp;

	@Column(name="po_purch_type")
	private String poPurchType;

	@Column(name="po_display_upto_date")
	private Date poDisplayUptoDate;

	@Column(name="po_plant")
	private String poPlant;

	@Column(name="po_purch_org")
	private String poPurchOrg;

	@Column(name="po_created_on")
	private Date poCreatedOn;

	@Column(name="po_created_by")
	private String poCreatedBy;

	@Column(name="po_posted_date")
	private Date poPostedDate;

	@Column(name="po_rel_status")
	private String poRelStatus;

	@Column(name="po_comp_code")
	private String poCompCode;

	@Column(name="po_status")
	private String poStatus;

	@Column(name="po_type")
	private String poType;

	@Column(name="po_notification_status")
	private String poNotificationStatus;

	@Column(name="po_vendor")
	private String poVendor;

	@Column(name="po_partner")
	private String poPartner;

	@Column(name="po_last_modified_date")
	private Date poLastModifiedDate;

	@Column(name="po_last_modified_by")
	private String poLastModifiedBy;

	@Column(name="po_payment_terms")
	private String poPaymentTerms;

	@Column(name="po_incoterms")
	private String poIncoterms;

	@Column(name="po_country_key")
	private String poCountryKey;

	@Column(name="po_tax_procedure")
	private String poTaxProcedure;

	@Column(name="po_tax_code")
	private String poTaxCode;

	@Column(name="po_advance_amt")
	private BigDecimal poAdvanceAmt;

	public ErpPoHeader()
	{
	}
	 
	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public BigInteger getErpPoHeaderId() {
		return erpPoHeaderId;
	}

	public void setErpPoHeaderId(BigInteger erpPoHeaderId) {
		this.erpPoHeaderId = erpPoHeaderId;
	}

	public String getPoPurchGrp() {
		return poPurchGrp;
	}

	public void setPoPurchGrp(String poPurchGrp) {
		this.poPurchGrp = poPurchGrp;
	}

	public String getPoPurchType() {
		return poPurchType;
	}

	public void setPoPurchType(String poPurchType) {
		this.poPurchType = poPurchType;
	}

	public Date getPoDisplayUptoDate() {
		return poDisplayUptoDate;
	}

	public void setPoDisplayUptoDate(Date poDisplayUptoDate) {
		this.poDisplayUptoDate = poDisplayUptoDate;
	}

	public String getPoPlant() {
		return poPlant;
	}

	public void setPoPlant(String poPlant) {
		this.poPlant = poPlant;
	}

	public String getPoPurchOrg() {
		return poPurchOrg;
	}

	public void setPoPurchOrg(String poPurchOrg) {
		this.poPurchOrg = poPurchOrg;
	}

	public Date getPoCreatedOn() {
		return poCreatedOn;
	}

	public void setPoCreatedOn(Date poCreatedOn) {
		this.poCreatedOn = poCreatedOn;
	}

	public String getPoCreatedBy() {
		return poCreatedBy;
	}

	public void setPoCreatedBy(String poCreatedBy) {
		this.poCreatedBy = poCreatedBy;
	}

	public Date getPoPostedDate() {
		return poPostedDate;
	}

	public void setPoPostedDate(Date poPostedDate) {
		this.poPostedDate = poPostedDate;
	}

	public String getPoRelStatus() {
		return poRelStatus;
	}

	public void setPoRelStatus(String poRelStatus) {
		this.poRelStatus = poRelStatus;
	}

	public String getPoCompCode() {
		return poCompCode;
	}

	public void setPoCompCode(String poCompCode) {
		this.poCompCode = poCompCode;
	}

	public String getPoStatus() {
		return poStatus;
	}

	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getPoNotificationStatus() {
		return poNotificationStatus;
	}

	public void setPoNotificationStatus(String poNotificationStatus) {
		this.poNotificationStatus = poNotificationStatus;
	}

	public String getPoVendor() {
		return poVendor;
	}

	public void setPoVendor(String poVendor) {
		this.poVendor = poVendor;
	}

	public String getPoPartner() {
		return poPartner;
	}

	public void setPoPartner(String poPartner) {
		this.poPartner = poPartner;
	}

	public Date getPoLastModifiedDate() {
		return poLastModifiedDate;
	}

	public void setPoLastModifiedDate(Date poLastModifiedDate) {
		this.poLastModifiedDate = poLastModifiedDate;
	}

	public String getPoLastModifiedBy() {
		return poLastModifiedBy;
	}

	public void setPoLastModifiedBy(String poLastModifiedBy) {
		this.poLastModifiedBy = poLastModifiedBy;
	}

	public String getPoPaymentTerms() {
		return poPaymentTerms;
	}

	public void setPoPaymentTerms(String poPaymentTerms) {
		this.poPaymentTerms = poPaymentTerms;
	}

	public String getPoIncoterms() {
		return poIncoterms;
	}

	public void setPoIncoterms(String poIncoterms) {
		this.poIncoterms = poIncoterms;
	}

	public String getPoCountryKey() {
		return poCountryKey;
	}

	public void setPoCountryKey(String poCountryKey) {
		this.poCountryKey = poCountryKey;
	}

	public String getPoTaxProcedure() {
		return poTaxProcedure;
	}

	public void setPoTaxProcedure(String poTaxProcedure) {
		this.poTaxProcedure = poTaxProcedure;
	}

	public String getPoTaxCode() {
		return poTaxCode;
	}

	public void setPoTaxCode(String poTaxCode) {
		this.poTaxCode = poTaxCode;
	}

	public BigDecimal getPoAdvanceAmt() {
		return poAdvanceAmt;
	}

	public void setPoAdvanceAmt(BigDecimal poAdvanceAmt) {
		this.poAdvanceAmt = poAdvanceAmt;
	}
}
