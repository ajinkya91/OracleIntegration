package com.muraai.oracleconnector.domain;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "erp_po_line")
public class ErpPoLine implements Serializable{ 

	private static final long serialVersionUID = 1L;

	@Column(name="line_no")
	private Integer lineNo;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="erp_po_line_id")
	private BigInteger erpPoLineId;

	@Column(name="line_po_no")
	private String linePoNo;

	@Column(name="line_status")
	private String lineStatus;

	@Column(name="line_asn_sel_status")
	private String lineAsnSelStatus;

	@Column(name="line_material_code")
	private String lineMaterialCode;

	@Column(name="line_lot")
	private Integer lineLot;

	@Column(name="line_material_desc")
	private String lineMaterialDesc;

	@Column(name="line_order_qty")
	private  BigDecimal lineOrderQty;

	@Column(name="line_uom")
	private String lineUom;

	@Column(name="line_revision")
	private String lineRevision;

	@Column(name="line_price")
	private  BigDecimal linePrice;

	@Column(name="line_currency")
	private String lineCurrency;

	@Column(name="line_excg_rate")
	private String lineExcgRate;

	@Column(name="line_temp_asnno")
	private String lineTempAsnno;

	@Column(name="line_last_modified_date")
	private Date lineLastModifiedDate;

	@Column(name="line_last_modified_by")
	private String lineLastModifiedBy;

	@Column(name="line_asned_qty")
	private BigDecimal lineAsnedQty;

	@Column(name="create_by")
	private String createBy;

	@Column(name="created_on")
	private Date createdOn;
	
	@Column (name = "line_gross_amt")
	private BigDecimal lineGrossAmt;
	
	@Column(name="line_doc_currency")
	private String lineDocCurrency;
	
	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public BigInteger getErpPoLineId() {
		return erpPoLineId;
	}

	public void setErpPoLineId(BigInteger erpPoLineId) {
		this.erpPoLineId = erpPoLineId;
	}

	public String getLinePoNo() {
		return linePoNo;
	}

	public void setLinePoNo(String linePoNo) {
		this.linePoNo = linePoNo;
	}

	public String getLineStatus() {
		return lineStatus;
	}

	public void setLineStatus(String lineStatus) {
		this.lineStatus = lineStatus;
	}

	public String getLineAsnSelStatus() {
		return lineAsnSelStatus;
	}

	public void setLineAsnSelStatus(String lineAsnSelStatus) {
		this.lineAsnSelStatus = lineAsnSelStatus;
	}

	public String getLineMaterialCode() {
		return lineMaterialCode;
	}

	public void setLineMaterialCode(String lineMaterialCode) {
		this.lineMaterialCode = lineMaterialCode;
	}

	public Integer getLineLot() {
		return lineLot;
	}

	public void setLineLot(Integer lineLot) {
		this.lineLot = lineLot;
	}

	public String getLineMaterialDesc() {
		return lineMaterialDesc;
	}

	public void setLineMaterialDesc(String lineMaterialDesc) {
		this.lineMaterialDesc = lineMaterialDesc;
	}

	public BigDecimal getLineOrderQty() {
		return lineOrderQty;
	}

	public void setLineOrderQty(BigDecimal lineOrderQty) {
		this.lineOrderQty = lineOrderQty;
	}

	public String getLineUom() {
		return lineUom;
	}

	public void setLineUom(String lineUom) {
		this.lineUom = lineUom;
	}

	public String getLineRevision() {
		return lineRevision;
	}

	public void setLineRevision(String lineRevision) {
		this.lineRevision = lineRevision;
	}

	public String getLineCurrency() {
		return lineCurrency;
	}

	public void setLineCurrency(String lineCurrency) {
		this.lineCurrency = lineCurrency;
	}

	public String getLineExcgRate() {
		return lineExcgRate;
	}

	public void setLineExcgRate(String lineExcgRate) {
		this.lineExcgRate = lineExcgRate;
	}

	public String getLineTempAsnno() {
		return lineTempAsnno;
	}

	public void setLineTempAsnno(String lineTempAsnno) {
		this.lineTempAsnno = lineTempAsnno;
	}

	public Date getLineLastModifiedDate() {
		return lineLastModifiedDate;
	}

	public void setLineLastModifiedDate(Date lineLastModifiedDate) {
		this.lineLastModifiedDate = lineLastModifiedDate;
	}

	public String getLineLastModifiedBy() {
		return lineLastModifiedBy;
	}

	public void setLineLastModifiedBy(String lineLastModifiedBy) {
		this.lineLastModifiedBy = lineLastModifiedBy;
	}

	public BigDecimal getLineAsnedQty() {
		return lineAsnedQty;
	}

	public void setLineAsnedQty(BigDecimal lineAsnedQty) {
		this.lineAsnedQty = lineAsnedQty;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public BigDecimal getLineGrossAmt() {
		return lineGrossAmt;
	}

	public void setLineGrossAmt(BigDecimal lineGrossAmt) {
		this.lineGrossAmt = lineGrossAmt;
	}

	@Override
	public String toString() {
		return "ErpPoLine [lineNo=" + lineNo + ", erpPoLineId=" + erpPoLineId + ", linePoNo=" + linePoNo
				+ ", lineStatus=" + lineStatus + ", lineAsnSelStatus=" + lineAsnSelStatus + ", lineMaterialCode="
				+ lineMaterialCode + ", lineLot=" + lineLot + ", lineMaterialDesc=" + lineMaterialDesc
				+ ", lineOrderQty=" + lineOrderQty + ", lineUom=" + lineUom + ", lineRevision=" + lineRevision
				+ ", linePrice=" + linePrice + ", lineCurrency=" + lineCurrency + ", lineExcgRate=" + lineExcgRate
				+ ", lineTempAsnno=" + lineTempAsnno + ", lineLastModifiedDate=" + lineLastModifiedDate
				+ ", lineLastModifiedBy=" + lineLastModifiedBy + ", lineAsnedQty=" + lineAsnedQty + ", createBy="
				+ createBy + ", createdOn=" + createdOn + ", lineDocCurrency=" + lineDocCurrency + "]";
	}

	public ErpPoLine(Integer lineNo, String lineMaterialCode, String lineMaterialDesc, BigDecimal lineOrderQty,
			String lineUom, BigDecimal linePrice, String lineCurrency, BigDecimal lineGrossAmt) {
		super();
		this.lineNo = lineNo;
		this.lineMaterialCode = lineMaterialCode;
		this.lineMaterialDesc = lineMaterialDesc;
		this.lineOrderQty = lineOrderQty;
		this.lineUom = lineUom;
		this.linePrice = linePrice;
		this.lineCurrency = lineCurrency;
		this.lineGrossAmt = lineGrossAmt;
	}
	
	public ErpPoLine() {
		super();
	}

	public BigDecimal getLinePrice() {
		return linePrice;
	}

	public void setLinePrice(BigDecimal linePrice) {
		this.linePrice = linePrice;
	}

	public String getLineDocCurrency() {
		return lineDocCurrency;
	}

	public void setLineDocCurrency(String lineDocCurrency) {
		this.lineDocCurrency = lineDocCurrency;
	}

}