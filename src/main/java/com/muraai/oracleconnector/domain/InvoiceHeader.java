package com.muraai.oracleconnector.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "invoice_header")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class InvoiceHeader implements Serializable {

	private static final long serialVersionUID = 1898883406239617516L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;

	@Column(name = "invoice_no")
	private String invoiceNo;

	@Column(name = "externalepeid")
	private String externalEPEID;

	@Column(name = "internal_doc_id")
	private BigInteger internalDocId;

	@Column(name = "document_name")
	private String documentName;

	@Column(name = "invoice_date")
	private Date invoiceDate;

	@Column(name = "invoice_po")
	private String invoicePo;

	@Column(name = "invoice_status")
	private String invoiceStatus;

	@Column(name = "invoice_value")
	private BigDecimal invoiceValue;
	
	@Column(name = "sub_total")
	private BigDecimal subTotal;
	

	@Column(name = "uploaded_by")
	private String uploadedBy;

	@Column(name = "taxcode")
	private String taxcode;

	@Column(name = "payment_terms")
	private String paymentTerms;

	@Column(name = "invoice_src")
	private String invoiceSrc;

	@Column(name = "occ_docid")
	private String occDocid;

	@Column(name = "two_way_matching_status")
	private String twoWayMatchingStatus;

	@Column(name = "three_way_matching_status")
	private String threeWayMatchingStatus;

	@Column(name = "requestor")
	private String requestor;

	@Column(name = "err_code")
	private String errCode;

	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "glcode")
	private String glcode;

	@Column(name = "costcenter")
	private String costcenter;

	@Column(name = "tax_per")
	private BigDecimal taxPer;

	@Column(name = "posting_doc_no")
	private String postingDocNo;

	@Column(name = "currency")
	private String currency;

	@Column(name = "invoice_discount_val")
	private BigDecimal invoiceDiscountVal;

	@Column(name = "invoice_cleared_amt")
	private BigDecimal invoiceClearedAmt;

	@Column(name = "due_date")
	private Date dueDate;

	@Column(name = "pending_with")
	private String pendingWith;

	@Column(name = "ship_to")
	private String shipTo;

	@Column(name = "billing_to")
	private String billingTo;
	
	@Column(name = "billing_address")
	private String billingAddress;

	@Column(name = "invoice_classification")
	private String invoiceClassification;

	@Column(name = "vendor_code")
	private String vendorCode;
	
	@Column(name = "vendor_name")
	private String vendorName;

	@Column(name = "entity_name")
	private String entityName;

	@Column(name = "entity_code")
	private String entityCode;

	@Column(name = "dc_no")
	private String dcNo;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "invoice_received_date")
	private Date invoiceReceivedDate;
	
	@Column(name = "posted_date")
	private Date postedDate;
	
	@Column(name = "scanned_no")
	private String scannedNo;
	
	@Column(name = "grand_total")
	private BigDecimal grossInvAmount;
	
	@Column(name = "processed_by")
	private String processedBy;
	
	@Column(name = "invoice_type")
	private String invoiceType;
	
	/*@OneToMany(mappedBy = "invoiceHeader",fetch = FetchType.LAZY)
	@JsonIgnore
	private List<InvoiceDetails> invoiceDetails;*/
	
	@Column(name = "frieght_amount")
	private BigDecimal freightAmount;
	
	/*public List<InvoiceDetails> getInvoiceDetails() {
		return invoiceDetails;
	}

	public void setInvoiceDetails(List<InvoiceDetails> invoiceDetails) {
		this.invoiceDetails = invoiceDetails;
	}
*/
	@Column(name = "inbound_invoice_doc_id",insertable = false,updatable = false)
	@JsonIgnore
	private Integer inboundInvoiceDocId;

	/*@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inbound_invoice_doc_id")
	private InboundInvoiceDoc inboundInvoiceDoc;*/
	
	/*@ManyToOne(fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name = "vendor_code", referencedColumnName="vendor_code",insertable = false,updatable = false)
	private VendorMaster vendorMaster;*/
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_po",referencedColumnName="po_no", insertable = false,updatable = false)
	private ErpPoHeader erpPoHeader;
	
	public ErpPoHeader getErpPoHeader() {
		return erpPoHeader;
	}

	public void setErpPoHeader(ErpPoHeader erpPoHeader) {
		this.erpPoHeader = erpPoHeader;
	}

	/*public VendorMaster getVendorMaster() {
		return vendorMaster;
	}

	public void setVendorMaster(VendorMaster vendorMaster) {
		this.vendorMaster = vendorMaster;
	}
*/
	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}

	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getExternalEPEID() {
		return externalEPEID;
	}

	public void setExternalEPEID(String externalEPEID) {
		this.externalEPEID = externalEPEID;
	}

	public BigInteger getInternalDocId() {
		return internalDocId;
	}

	public void setInternalDocId(BigInteger internalDocId) {
		this.internalDocId = internalDocId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoicePo() {
		return invoicePo;
	}

	public void setInvoicePo(String invoicePo) {
		this.invoicePo = invoicePo;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public BigDecimal getInvoiceValue() {
		return invoiceValue;
	}

	public void setInvoiceValue(BigDecimal invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public String getTaxcode() {
		return taxcode;
	}

	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getInvoiceSrc() {
		return invoiceSrc;
	}

	public void setInvoiceSrc(String invoiceSrc) {
		this.invoiceSrc = invoiceSrc;
	}

	public String getOccDocid() {
		return occDocid;
	}

	public void setOccDocid(String occDocid) {
		this.occDocid = occDocid;
	}

	public String getTwoWayMatchingStatus() {
		return twoWayMatchingStatus;
	}

	public void setTwoWayMatchingStatus(String twoWayMatchingStatus) {
		this.twoWayMatchingStatus = twoWayMatchingStatus;
	}

	public String getThreeWayMatchingStatus() {
		return threeWayMatchingStatus;
	}

	public void setThreeWayMatchingStatus(String threeWayMatchingStatus) {
		this.threeWayMatchingStatus = threeWayMatchingStatus;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getGlcode() {
		return glcode;
	}

	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}

	public String getCostcenter() {
		return costcenter;
	}

	public void setCostcenter(String costcenter) {
		this.costcenter = costcenter;
	}

	public BigDecimal getTaxPer() {
		return taxPer;
	}

	public void setTaxPer(BigDecimal taxPer) {
		this.taxPer = taxPer;
	}

	public String getPostingDocNo() {
		return postingDocNo;
	}

	public void setPostingDocNo(String postingDocNo) {
		this.postingDocNo = postingDocNo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getInvoiceDiscountVal() {
		return invoiceDiscountVal;
	}

	public void setInvoiceDiscountVal(BigDecimal invoiceDiscountVal) {
		this.invoiceDiscountVal = invoiceDiscountVal;
	}

	public BigDecimal getInvoiceClearedAmt() {
		return invoiceClearedAmt;
	}

	public void setInvoiceClearedAmt(BigDecimal invoiceClearedAmt) {
		this.invoiceClearedAmt = invoiceClearedAmt;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getPendingWith() {
		return pendingWith;
	}

	public void setPendingWith(String pendingWith) {
		this.pendingWith = pendingWith;
	}

	public String getShipTo() {
		return shipTo;
	}

	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	public String getBillingTo() {
		return billingTo;
	}

	public void setBillingTo(String billingTo) {
		this.billingTo = billingTo;
	}

	public String getInvoiceClassification() {
		return invoiceClassification;
	}

	public void setInvoiceClassification(String invoiceClassification) {
		this.invoiceClassification = invoiceClassification;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public String getDcNo() {
		return dcNo;
	}

	public void setDcNo(String dcNo) {
		this.dcNo = dcNo;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}



	/*public InboundInvoiceDoc getInboundInvoiceDoc() {
		return inboundInvoiceDoc;
	}

	public void setInboundInvoiceDoc(InboundInvoiceDoc inboundInvoiceDoc) {
		this.inboundInvoiceDoc = inboundInvoiceDoc;
	}
*/
	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Integer getInboundInvoiceDocId() {
		return inboundInvoiceDocId;
	}

	public void setInboundInvoiceDocId(Integer inboundInvoiceDocId) {
		this.inboundInvoiceDocId = inboundInvoiceDocId;
	}

	public Date getInvoiceReceivedDate() {
		return invoiceReceivedDate;
	}

	public void setInvoiceReceivedDate(Date invoiceReceivedDate) {
		this.invoiceReceivedDate = invoiceReceivedDate;
	}

	public Date getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}

	public String getScannedNo() {
		return scannedNo;
	}

	public void setScannedNo(String scannedNo) {
		this.scannedNo = scannedNo;
	}

	public BigDecimal getGrossInvAmount() {
		return grossInvAmount;
	}

	public void setGrossInvAmount(BigDecimal grossInvAmount) {
		this.grossInvAmount = grossInvAmount;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public BigDecimal getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(BigDecimal freightAmount) {
		this.freightAmount = freightAmount;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	
 
}