package com.muraai.oracleconnector.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_details")
public class InvoiceDetails implements Serializable {

	private static final long serialVersionUID = -1370973183270326037L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoice_details_id")
	private Integer invoiceDetailsId;

	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;

	@Column(name = "line_no")
	private Integer lineNo;

	@Column(name = "material")
	private String material;

	@Column(name = "material_desc")
	private String materialDesc;

	@Column(name = "qty")
	private BigDecimal qty;

	@Column(name = "rate")
	private BigDecimal rate;

	@Column(name = "curr")
	private String curr;

	@Column(name = "glcode")
	private String glcode;

	@Column(name = "costcenter")
	private String costcenter;

	@Column(name = "tax")
	private BigDecimal tax;

	@Column(name = "tax_type")
	private String taxType;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "modified_on")
	private Date modifiedOn;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "invoice_no")
	private String invoiceNo;

	@Column(name = "invoice_po")
	private String invoicePo;

	@Column(name = "po_line_no")
	private String polineNo;

	@Column(name = "grn_no")
	private Integer grnNo;

	@Column(name = "LineTotal")
	private BigDecimal lineTotal;
	
// for non po
	@Column(name = "requestor")
	private String requestor;

	@Column(name = "approver")
	private String approver;
	
	@Column(name = "gross_amount")
	private BigDecimal grossAmount;
	
	@Column(name = "discount")
	private BigDecimal discount;
	
	
	@Column(name = "approval_status")
	private String  status;
	
	@Column(name = "order_id")
	private String  orderId;
	
	@Column(name = "remarks")
	private String  remarks;
	
	@Column(name = "uom")
	private String uom;

	@Column(name = "validator_remarks")
	private String validatorRemarks;
	
	@Column(name = "tax_jurisdctn")
	private String taxJurisdctn;
	
	@Column(name = "line_status")
	private String lineStatus;
	
/*	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		   @JoinColumn(name = "invoice_po", referencedColumnName = "line_po_no",insertable = false, updatable = false ),
		   @JoinColumn(name = "line_no", referencedColumnName = "line_no",insertable = false, updatable = false ) })
    private ErpPoLine erpPoLine;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "invoice_po", referencedColumnName = "grn_po_no", insertable = false, updatable = false),
		@JoinColumn(name = "line_no", referencedColumnName = "grn_po_line_no", insertable = false, updatable =  false) })
	private ErpGoodsReciept erpGoodsReciept;
*/	
	
	@Column(name = "comp_code")
	private String compCode;

/*	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_header_id", insertable = false, updatable = false)
	@ApiModelProperty(notes = "invoice header id")
	@JsonIgnore
	private InvoiceHeader invoiceHeader;
*/	
	@Column(name = "buyer")
	private String  buyer;
	
/*	public InvoiceHeader getInvoiceHeader() {
		return invoiceHeader;
	}

	public void setInvoiceHeader(InvoiceHeader invoiceHeader) {
		this.invoiceHeader = invoiceHeader;
	}
*/
	public Integer getInvoiceDetailsId() {
		return invoiceDetailsId;
	}
	public Integer getInvoiceDetailsId(InvoiceDetails details) {
		return details.invoiceDetailsId;
	}

	public void setInvoiceDetailsId(Integer invoiceDetailsId) {
		this.invoiceDetailsId = invoiceDetailsId;
	}

	public Integer getInvoiceHeaderId(InvoiceDetails details) {
		return details.invoiceHeaderId;
	}
	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}
	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}
	public String getMaterial(InvoiceDetails details) {
		return details.material;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}

	public String getMaterialDesc() {
		return materialDesc;
	}
	public String getMaterialDesc(InvoiceDetails details) {
		return details.materialDesc;
	}

	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}

	public BigDecimal getRate() {
		return rate;
	}
	public BigDecimal getRate(InvoiceDetails details) {
		return details.rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getCurr() {
		return curr;
	}
	public String getCurr(InvoiceDetails details) {
		return details.curr;
	}

	public void setCurr(String curr) {
		this.curr = curr;
	}

	public String getGlcode() {
		return glcode;
	}
	public String getGlcode(InvoiceDetails details) {
		return details.glcode;
	}
	public void setGlcode(String glcode) {
		this.glcode = glcode;
	}

	public String getCostcenter() {
		return costcenter;
	}
	public String getCostcenter(InvoiceDetails details) {
		return details.costcenter;
	}
	public void setCostcenter(String costcenter) {
		this.costcenter = costcenter;
	}

	public BigDecimal getTax() {
		return tax;
	}
	public BigDecimal getTax(InvoiceDetails details) {
		return details.tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public String getTaxType(InvoiceDetails details) {
		return details.taxType;
	}
	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Date getCreatedOn() {
		return createdOn;
	}
	public Date getCreatedOn(InvoiceDetails details) {
		return details.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}
	public String getCreatedBy(InvoiceDetails details) {
		return details.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}
	public Date getModifiedOn(InvoiceDetails details) {
		return details.modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}
	public String getModifiedBy(InvoiceDetails details) {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}
	public String getInvoiceNo(InvoiceDetails details) {
		return details.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoicePo() {
		return invoicePo;
	}
	public String getInvoicePo(InvoiceDetails details) {
		return details.invoicePo;
	}

	public void setInvoicePo(String invoicePo) {
		this.invoicePo = invoicePo;
	}
/*	public ErpPoLine getErpPoLine() {
		return erpPoLine;
	}

	public void setErpPoLine(ErpPoLine erpPoLine) {
		this.erpPoLine = erpPoLine;
	}
	public ErpGoodsReciept getErpGoodsReciept() {
		return erpGoodsReciept;
	}
	 
	public void setErpGoodsReciept(ErpGoodsReciept erpGoodsReciept) {
		this.erpGoodsReciept = erpGoodsReciept;
	}
*/
	public String getPolineNo() {
		return polineNo;
	}
	public String getPolineNo(InvoiceDetails details) {
		return details.polineNo;
	}
	public void setPolineNo(String polineNo) {
		this.polineNo = polineNo;
	}

	public String getUom() {
		return uom;
	}
	public String getUom(InvoiceDetails details) {
		return details.uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}

	public Integer getGrnNo() {
		return grnNo;
	}
	public Integer getGrnNo(InvoiceDetails details) {
		return details.grnNo;
	}
	public void setGrnNo(Integer grnNo) {
		this.grnNo = grnNo;
	}

	public BigDecimal getLineTotal() {
		return lineTotal;
	}
	public BigDecimal getLineTotal(InvoiceDetails details) {
		return details.lineTotal;
	}
	public void setLineTotal(BigDecimal lineTotal) {
		this.lineTotal = lineTotal;
	}

	public BigDecimal getQty() {
		return qty;
	}
	public BigDecimal getQty(InvoiceDetails details) {
		return details.qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public Integer getLineNo() {
		return lineNo;
	}
	public Integer getLineNo(InvoiceDetails details) {
		return details.lineNo;
	}
	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public String getValidatorRemarks() {
		return validatorRemarks;
	}
	public String getValidatorRemarks(InvoiceDetails details) {
		return details.validatorRemarks;
	}
	public void setValidatorRemarks(String validatorRemarks) {
		this.validatorRemarks = validatorRemarks;
	}

	public String getRequestor() {
		return requestor;
	}
	public String getRequestor(InvoiceDetails details) {
		return details.requestor;
	}
	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getApprover() {
		return approver;
	}
	public String getApprover(InvoiceDetails details) {
		return details.approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}
	public String getStatus() {
		return status;
	}
	public String getStatus(InvoiceDetails details) {
		return details.status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderId() {
		return orderId;
	}
	public String getOrderId(InvoiceDetails details) {
		return details.orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRemarks() {
		return remarks;
	}
	public String getRemarks(InvoiceDetails details) {
		return details.remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTaxJurisdctn() {
		return taxJurisdctn;
	}
	public String getTaxJurisdctn(InvoiceDetails details) {
		return details.taxJurisdctn;
	}
	public void setTaxJurisdctn(String taxJurisdctn) {
		this.taxJurisdctn = taxJurisdctn;
	}

	public String getLineStatus(InvoiceDetails details) {
		return details.lineStatus;
	}
	public String getLineStatus() {
		return lineStatus;
	}

	public void setLineStatus(String lineStatus) {
		this.lineStatus = lineStatus;
	}

	public BigDecimal getGrossAmount() {
		return grossAmount;
	}
	public BigDecimal getGrossAmount(InvoiceDetails details) {
		return details.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public BigDecimal getDiscount() {
		return discount;
	}
	public BigDecimal getDiscount(InvoiceDetails details) {
		return details.discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}
    
}
