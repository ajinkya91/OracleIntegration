package com.muraai.oracleconnector;

public class Constant {
	public static final String HIBERNATE_DIALECT ="hibernate.dialect";
	public static final String HIBERNATE_HBM2_DDL ="hibernate.hbm2ddl.auto";
	public static final String HIBERNATE_EJB_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
	public static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
	public static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	public static final String CONTROLLER_PACKAGE_NAME="com.muraai.oracleconnector.web";
	public static final String REPOSITORY_PACKAGE_NAME="com.muraai.oracleconnector.repository";
	public static final String MODEL_PACKAGE_NAME="com.muraai.oracleconnector.domain";
}
