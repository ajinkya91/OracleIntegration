package com.muraai.oracleconnector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.muraai.oracleconnector.domain.ErpPoLine;

@Repository
public interface ErpPoLineRepository extends CrudRepository<ErpPoLine, Integer> {

	@Query("select e from ErpPoLine e where e.linePoNo = :linePoNo")
	public List<ErpPoLine> getErpPoLinePoNo(@Param("linePoNo")String linePoNo);
}
