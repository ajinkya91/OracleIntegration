package com.muraai.oracleconnector.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.muraai.oracleconnector.domain.InvoiceHeader;

@Repository
public interface InvoiceHeaderRepository extends CrudRepository<InvoiceHeader, Integer> {

	
}