package com.muraai.oracleconnector.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.muraai.oracleconnector.domain.InvoiceDetails;

@Repository
public interface InvoiceDetailsRepository extends CrudRepository<InvoiceDetails, Integer> {

		
	List<InvoiceDetails> findByInvoiceHeaderId(Integer invoiceHeaderId);
	
	}
