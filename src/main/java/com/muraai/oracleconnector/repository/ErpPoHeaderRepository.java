package com.muraai.oracleconnector.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.muraai.oracleconnector.domain.ErpPoHeader;

@Repository
public interface ErpPoHeaderRepository extends CrudRepository<ErpPoHeader,BigInteger> {
	@Query("select   erp  from ErpPoHeader erp  where erp.poNo=:poNumber")
	List<ErpPoHeader> getErpPoHeaderDetailByPoNumber(@Param("poNumber")String poNumber);
}
