package com.muraai.oracleconnector.web;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.muraai.oracleconnector.domain.ErpPoHeader;
import com.muraai.oracleconnector.domain.ErpPoLine;
import com.muraai.oracleconnector.domain.InvoiceDetails;
import com.muraai.oracleconnector.domain.InvoiceHeader;
import com.muraai.oracleconnector.repository.ErpPoHeaderRepository;
import com.muraai.oracleconnector.repository.ErpPoLineRepository;
import com.muraai.oracleconnector.repository.InvoiceDetailsRepository;
import com.muraai.oracleconnector.repository.InvoiceHeaderRepository;


@RestController
@RequestMapping("/rest")
public class ApplicationController {
	
	@Autowired
	Environment env;

	@Autowired
	ErpPoHeaderRepository erpPoheaderRepository;
	
	@Autowired
	ErpPoLineRepository erpPoLineRepository;
	
	@Autowired
	InvoiceHeaderRepository invoiceHeaderRepository;
	
	@Autowired
	InvoiceDetailsRepository invoiceDetailsRepository;

	
	@RequestMapping(value = "/services/postNonPo", method = RequestMethod.GET)
	@ResponseBody
	@Transactional(rollbackFor=Exception.class)
	public void invoicePosting(@RequestParam("invoiceHeaderId")Integer invoiceHeaderId) throws Exception{
		
		InvoiceHeader invHeader = invoiceHeaderRepository.findOne(invoiceHeaderId);
		Connection con = getConnection();
		Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		String query ="select pv.vendor_id,pvs.party_site_id vendor_site_id,pvs.org_id,segment1 vendor_code,vendor_name name,pvs.vendor_site_code plant,pvs.address_line1,pvs.address_line2,pvs.address_line3,pvs.address_line4,pvs.city,pvs.state,pvs.zip,pvs.country,pvs.area_code||'-'||pvs.phone phone,pvs.fax_area_code||'-'||pvs.fax fax,pvs.email_address,(select organization_code from mtl_parameters where organization_id=hou.organization_id) purchasing_org,hou.name purchasing_org_1,atp.name payment_term_value,atp.description payment_term_description from apps.ap_suppliers pv, apps.ap_supplier_sites_all pvs,apps.hz_party_sites hps,apps.hr_all_organization_units hou,apps.ap_terms_tl atp where 1 = 1 and pvs.vendor_id = pv.vendor_id and hps.party_site_id = pvs.party_site_id and pvs.org_id = hou.organization_id and pvs.terms_id = atp.term_id and atp.language='US'and vendor_name ='"+ invHeader.getVendorName()+ "'";
		ResultSet rs=stmt.executeQuery(query);
		System.out.println("Result set " + rs);
		insertIntoApInvoiceInterface(rs,invHeader,con);
		insertIntoApInvoiceLinesInterface(invHeader,con);
		selectResponsibility(rs,con);
	}

@Transactional(rollbackFor=Exception.class)
private void selectResponsibility(ResultSet rs, Connection con) throws Exception{

	String purchasingOrg =null;
	while(rs.previous()){
		purchasingOrg = rs.getString("purchasing_org_1");
	}
	String sql = "SELECT frv.responsibility_name FROM apps.fnd_profile_options fp,"
			+ "apps.fnd_profile_options_tl fpo,apps.fnd_responsibility_tl frv,apps.fnd_profile_option_values fpov,apps.hr_all_organization_units hou "
			+ "WHERE hou.NAME ='" +purchasingOrg + "'"
					+ "AND TO_NUMBER(fpov.profile_option_value) = hou.organization_id AND fp.profile_option_id = fpov.profile_option_id AND fp.profile_option_name = fpo.profile_option_name "
					+ "AND fpo.user_profile_option_name = 'MO: Operating Unit' AND frv.responsibility_id = fpov.level_value AND frv.application_id=200 AND fpo.language='US' AND frv.language='US' AND frv.responsibility_name like '%SUPER%USER%'";
	Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	ResultSet rsnew=stmt.executeQuery(sql);
	while(rsnew.next()){
	System.out.println("Successfully Executed ,Responsibility Name" + rsnew.getString("responsibility_name"));
	}
}

	@Transactional(rollbackFor=Exception.class)
private void insertIntoApInvoiceLinesInterface(InvoiceHeader invHeader, Connection con) throws Exception{
		
		List<InvoiceDetails> invoiceDetails = invoiceDetailsRepository.findByInvoiceHeaderId(invHeader.getInvoiceHeaderId());
		if(invoiceDetails !=null && invoiceDetails.size() >0){
			for(InvoiceDetails invDet : invoiceDetails){
				String sql = "insert into AP_INVOICE_LINES_INTERFACE (invoice_id,line_number,line_type_lookup_code,amount)"
						+ "values ((select MAX(invoice_id) from AP_INVOICES_INTERFACE where invoice_num=" + "'"+invHeader.getInvoiceNo()+"')"
								+ ","+  invDet.getLineNo()+",'ITEM',"+ invDet.getGrossAmount()+")";
				Statement stmt = con.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("insertIntoApInvoiceLinesInterface for line no " + invDet.getLineNo());
			}
		}
	}

	@Transactional(rollbackFor=Exception.class)
private void insertIntoApInvoiceInterface(ResultSet rs, InvoiceHeader invHeader, Connection con) throws Exception{
		String vendorId =null;
		String vendorSiteId =null;
		String orgId =null;
		while(rs.next()){
			vendorId =rs.getString("vendor_id");
			vendorSiteId =rs.getString("vendor_site_id");
			orgId =rs.getString("org_id");
		}
		String sql = "insert into AP_INVOICES_INTERFACE(invoice_id,invoice_num,vendor_id,vendor_site_id,invoice_amount,INVOICE_CURRENCY_CODE,invoice_date,DESCRIPTION,PAY_GROUP_LOOKUP_CODE,source,org_id)"
				+ "values(ap_invoices_interface_s.nextval,"+ "'"+ invHeader.getInvoiceNo() + "'" + ","+ vendorId + "," +vendorSiteId + "," + invHeader.getInvoiceValue() + ","+  "'" + invHeader.getCurrency() + "'" + ",'01-05-18',"
	            + "'This Invoice is created for test purpose',(select pay_group_lookup_code from apps.ap_supplier_sites_all where vendor_id=" + vendorId + " and vendor_site_id=" + vendorSiteId +
				"),(SELECT lookup_code FROM fnd_lookup_values WHERE lookup_type = 'SOURCE' and view_application_id=200 and lookup_code= 'MANUAL INVOICE ENTRY' and language='US')," + orgId +")";
		Statement stmt = con.createStatement();
		stmt.executeUpdate(sql);
		System.out.println("insertIntoApInvoiceInterface Successfull");
		
	}


@RequestMapping(value = "/services/pullPo", method = RequestMethod.GET)
@ResponseBody
@Transactional(rollbackFor=Exception.class)
public void pullPo(@RequestParam("poNumber") String poNumber) throws Exception{

		Connection con = getConnection();
		Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		String query ="SELECT poh.segment1 po_number,poh.approved_date release_date,ood.organization_name plant,hrls.location_code loc,(select name from apps.hr_operating_units where organization_id = poh.org_id) company_code,poh.type_lookup_code Document_Type,poh.authorization_status status,poh.creation_date created_on,ppf.full_name created_by,pov.vendor_name vendor,pov.vendor_id vendor_code,poh.created_language po_language,ats.description payment_term_description,ats.name payment_term_value,NVL(poh.rate,1) rate,poh.currency_code po_currency,hrls.country country_key,pol.tax_name tax_code,poh.note_to_vendor special_instruction,pol.LINE_NUM po_line_num,pol.closed_code po_line_status,msi.segment1 material_code,msi.description material_desc,pol.quantity line_quantity,pol.base_uom,NULL po_line_rev,pol.unit_price,pol.last_update_date,pod.destination_subinventory,(select segment4 from apps.gl_code_combinations where code_combination_id= pod.code_combination_id) gl_code,(select segment4 from apps.gl_code_combinations where code_combination_id= pod.code_combination_id) cost_center,povs.vendor_site_code,povs.ADDRESS_LINE1,povs.ADDRESS_LINE2,povs.ADDRESS_LINE3,povs.city,povs.state,povs.zip,povs.country FROM apps.PO_HEADERS_ALL poh,apps.ap_suppliers pov,apps.per_all_people_f ppf,apps.ap_terms ats,apps.hr_locations_all hrls,apps.PO_LINES_ALL pol,apps.mtl_system_items_b msi,apps.org_organization_definitions ood,apps.po_distributions_all pod,apps.ap_supplier_sites_all povs WHERE 1=1 AND pol.closed_code ='OPEN' AND pov.vendor_id = poh.vendor_id AND poh.agent_id  = ppf.person_id AND poh.terms_id = ats.term_id AND poh.ship_to_location_id = hrls.location_id AND poh.po_header_id= pol.po_header_id AND pol.item_id = msi.inventory_item_id AND pod.po_header_id = poh.po_header_id AND pod.po_line_id = pol.po_line_id AND ood.organization_id = pod.destination_organization_id AND pod.destination_organization_id = msi.organization_id AND povs.vendor_site_id = poh.vendor_site_id AND poh.approved_date >= poh.approved_date AND ood.organization_name is not null AND poh.segment1 ="+ "'"+poNumber+"'";
		ResultSet rs=stmt.executeQuery(query);
		saveErpPoLine(rs,poNumber);
		saveErpPoHeader(rs,poNumber);
		
	
	}

@Transactional(rollbackFor=Exception.class)
public Connection getConnection() throws Exception{
	
	Class.forName("oracle.jdbc.driver.OracleDriver");
	Connection con=DriverManager.getConnection("jdbc:oracle:thin:@172.16.101.92:1551:test1","apps","testapps"); 
	return con;
}


private void saveErpPoLine(ResultSet rs, String poNumber) throws Exception{
	
	List<ErpPoLine> erpPoLines = erpPoLineRepository.getErpPoLinePoNo(poNumber);
	if(erpPoLines !=null && erpPoLines.size()==0)
	{
	while(rs.next()){
		ErpPoLine poLine = new ErpPoLine();
		poLine.setLinePoNo(rs.getString("po_number"));
		poLine.setLineOrderQty(rs.getBigDecimal("line_quantity"));
		poLine.setLineUom(StringUtils.hasText(rs.getString("base_uom"))?rs.getString("base_uom"):"PC");
		poLine.setLineMaterialCode(rs.getString("material_code"));
		poLine.setLineMaterialDesc(rs.getString("material_desc"));
		poLine.setLinePrice(rs.getBigDecimal("UNIT_PRICE"));
		poLine.setLineCurrency(rs.getString("po_currency"));
		poLine.setLineNo(rs.getInt("po_line_num"));
		BigDecimal qty = rs.getBigDecimal("line_quantity");
		BigDecimal unitPrice = rs.getBigDecimal("UNIT_PRICE");
		BigDecimal grossAmt =qty.multiply(unitPrice); 
		poLine.setLineGrossAmt(grossAmt);
		poLine.setLineStatus(rs.getString("po_line_status"));
		erpPoLineRepository.save(poLine);
		System.out.println("Save erp po line for line no" + poLine.getLineNo());
	}
	}
}


private void saveErpPoHeader(ResultSet rs, String poNumber) throws Exception {
	List<ErpPoHeader> poHeaderList = erpPoheaderRepository.getErpPoHeaderDetailByPoNumber(poNumber);
	if(poHeaderList !=null && poHeaderList.size()==0)
	{
	while(rs.previous()){
	ErpPoHeader header = new ErpPoHeader();
	header.setPoNo(rs.getString("po_number"));
	header.setPoCreatedOn(rs.getDate("created_on"));
	header.setPoVendor(rs.getString("vendor_code"));
	header.setPoType(rs.getString("Document_Type"));
	header.setPoStatus(rs.getString("status"));
	header.setPoCountryKey(rs.getString("country_key"));
	header.setPoPaymentTerms(rs.getString("payment_term_description"));
	header.setPoCreatedBy(rs.getString("created_by"));
	erpPoheaderRepository.save(header);
	System.out.println("Save erp po header ");
	break;
	}
	}
}

}
